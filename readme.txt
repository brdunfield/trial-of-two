Project Name: Trial of Two
Pandacodium 2013
Team Members: Bethany Dunfield, Andrew Brough
Location: Ottawa, ON

Trial of Two is a two player game where players must solve a puzzle and go through their doors to complete the level. One player must push a pillar so that the other can cross gaps in the area, push a switch and open the other player's door. When both doors are open, and both players are standing in their doorways, you win.

You are always the blue character, and the other person is always the red character.

JQuery is used for animation of the menu UI.
Google Fonts is used for the menu UI font.
Node.js is the server side of the game.
Socket.io is used to facilitate the web socket between client and server.