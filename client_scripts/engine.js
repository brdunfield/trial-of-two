var TILE_WIDTH = 100;
var CANVAS_WIDTH = 1800;
var CANVAS_HEIGHT = 1500;
// Engine Constructor
var Engine = function(canvasID, dataObj) {
	var canvas = document.getElementById(canvasID),
		self = this;	
	canvas.width = CANVAS_WIDTH;
	canvas.height = CANVAS_HEIGHT;
	// virtual canvases for collision detection
	var canvas_0 = document.createElement('canvas'),
		canvas_1 = document.createElement('canvas');
	canvas_0.width = CANVAS_WIDTH;
	canvas_0.height = CANVAS_HEIGHT;
	canvas_1.width = CANVAS_WIDTH;
	canvas_1.height = CANVAS_HEIGHT;
	
	this.context = canvas.getContext('2d');
	this.layer0Context = canvas_0.getContext('2d');
	this.layer1Context = canvas_1.getContext('2d');
	
	// players
	var playerLayer = dataObj.playerIndex,
		remoteLayer = (dataObj.playerIndex === 0) ? 1 : 0;
	this.localPlayer = new Player(playerLayer, dataObj.playerStartCoords[playerLayer].x, dataObj.playerStartCoords[playerLayer].y);
	this.remotePlayer = new Player(remoteLayer, dataObj.playerStartCoords[remoteLayer].x, dataObj.playerStartCoords[remoteLayer].y);
	
	// World objects
	this.worldObjects = [];
	for (var i=0; i < dataObj.worldObjects.length; i++) {
		var obj = dataObj.worldObjects[i];
		switch (obj.type) {
			case 'pillar':
				this.worldObjects.push(new Pillar(obj.layer, obj.x, obj.y));
				break;
			case 'button':
				this.worldObjects.push(new Button(obj.x, obj.y, obj.layer));
				break;
			case 'door':
				this.worldObjects.push(new Door(obj.x, obj.y, obj.layer, obj.isOpen));
				break;
		}
	}
	
	this.goal = false;
	
	// key handlers
	this.keyHandlers = [];
	window.onkeypress = function (e) { self.keyPressed(e) };
	window.onkeydown = function (e) { self.keyPressed(e); };
	
	// Image assets
	this.world = dataObj.world;
	this.images = {};
	this.imagesLoaded = 0;
	this.imagesFailed = 0;
	this.imageUrls = [];
	this.imagesIndex = 0;
	
	// time
	this.startTime = 0;
	this.lastTime = 0;
	this.gameTime = 0;
	this.fps = 60;
	
	return this;
}

// Engine Methods
Engine.prototype = {
	// Start Loop
	start: function() {
		var self = this;
		this.startTime = this.getCurrentTime();
		console.log("Starting Game at: " + this.startTime);
		// draw black and white maps to virtual canvases
		this.updatePMaps();
		
		window.requestAnimationFrame(function(time) {
			self.animate.call(self, time);
		});
	},
	// Game Loop
	animate: function(time) {
		//console.log(time);
		var self = this;
		// Tick
		this.tick(time);
		// Update everything
		this.moveObject(this.localPlayer);
		this.moveObject(this.remotePlayer);
		for (var i = 0; i < this.worldObjects.length; i++)
		{
			if (this.worldObjects[i].movable) this.moveObject(this.worldObjects[i]);
		}
		
		// Clear screen
		this.clearScreen();
		// Draw both player layers
		this.drawLayer(0);
		
		this.drawLayer(1);
		
		// Paint over characters		
		
		// call next frame
		window.requestAnimationFrame(function(time) {
			self.animate.call(self, time);
		});
	},
	
	// Time
	getCurrentTime: function() {
		return new Date().getTime();	
	},	
	tick: function(time) {
		// update fps
		if (this.lastTime === 0) { this.fps = 60; }
		else { this.fps = Math.round(1000 / (time - this.lastTime)); }
		// update Time
		this.lastTime = time;
		//this.gameTime = this.getCurrentTime() - this.startTime;
		
		//document.getElementById("fps").innerHTML = this.fps;
	},
	
	// Updating functions
	moveObject: function(obj) {
		if (!obj.moving) return;
		
		var dist = (obj.vel / this.fps);
		
		switch (obj.dir) {
			case 'up':
				obj.y -= dist;
				break;
			case 'down':
				obj.y += dist;
				break;
			case 'left':
				obj.x -= dist;
				break;
			case 'right':
				obj.x += dist;
				break;
		}
		if (obj == this.localPlayer)
			this.checkObjects(obj, dist);
		if(obj.movable) {
			this.updatePMaps();
		}
		
	},
	
	// Collision Detection - use circular bounding areas for now
	checkObjects: function(obj, dist) {
		
		// check against all objects
		for (var i = 0; i < this.worldObjects.length; i++) {
			var wObj = this.worldObjects[i];
			if (!(wObj.layer === obj.layer)) continue;
			var distance = Math.sqrt(
				Math.pow(wObj.x - this.localPlayer.x, 2) +
				Math.pow(wObj.y - this.localPlayer.y, 2));
			if (distance < 50)
				switch(wObj.type) {
					case 'pillar':
						if (!wObj.moving) {
							if (this.verifyMove(wObj, obj.dir, 100)) {
								wObj.move(obj.dir);
								this.localPlayer.pushing = true;
								console.log("Pillar moved");
								socket.emit("move", {'object': 'worldObject', 'index': i, 'dir': obj.dir});
							}
						};	
						break;
					case 'button':
						if (!wObj.pressed) {
							wObj.activate();
							// activate any toggled doors
							this.activateDoors(wObj.linkedDoorsID);						
							console.log("Button pressed");
							socket.emit("buttonPressed", {'object': 'worldObject', 'index': i, "id": wObj.linkedDoorsID});
						}
						break;
					case 'door':
						if (!this.goal) {			
							this.goal = true;
							console.log("Reached Goal!");
							socket.emit("reachedGoal");
						}
						break;
				}
		}
	},
	
	activateDoors: function(id) {
		for (var j = 0; j < this.worldObjects.length; j++) {
			var dObj = this.worldObjects[j];
			if (dObj.type == 'door')
				if (dObj.linkedDoorsID == id) dObj.activate();
		}
	},
	
	verifyMove: function(obj, dir, dist) {	
		var x = obj.x, y = obj.y;
		var layer = obj.layer;
		switch (dir) {
			case 'up':
				y -= dist;
				break;
			case 'down':
				y += dist;
				break;
			case 'left':
				x -= dist;
				break;
			case 'right':
				x += dist;
				break;
		}
		if (x < 0 || x > CANVAS_WIDTH || y < 0 || y > CANVAS_HEIGHT) return false;
		// select the proper map based on layer
		var pixel = this["layer" + layer + "Context"].getImageData(x, y, 1, 1).data;
		return (!(pixel[0] === 0)); // returns true if white, false if black;
	},
	
	// Drawing functions
	clearScreen: function() {
		this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
	},
	
	drawLayer: function(l) {
		// draw background
		this.context.drawImage(this.getImage("map_layer" + l), 0 ,0);
		// draw objects behind player
		var p = (l === this.localPlayer.layer) ? this.localPlayer : this.remotePlayer;
		for (var i=0; i < this.worldObjects.length; i++) {
			var obj = this.worldObjects[i];
			if (!obj.layer == l) continue;
			if (obj.y <= p.y)
				switch(obj.type) {
					case 'pillar':
						this.drawPillar(obj);
						break;
					case 'button':
						this.drawButton(obj);
						break;
					case 'door':
						this.drawDoor(obj);
						break;
				}
		}			
		
		// draw player
		this.drawPlayer(p);
		
		// draw objects in front of player
		for (var i=0; i < this.worldObjects.length; i++) {
			var obj = this.worldObjects[i];
			if (!obj.layer == l) continue;
			if (obj.y > p.y)
				switch(obj.type) {
					case 'pillar':
						this.drawPillar(obj);
						break;
					case 'button':
						this.drawButton(obj);
						break;
					case 'door':
						this.drawDoor(obj);
						break;
				}
		}		
		
	},
	// Object methods
	drawDebug: function( obj) {
		this.context.strokeStyle = "#000";
		this.context.beginPath();
		this.context.rect(obj.x - 50, obj.y - 50, 100, 100);
		this.context.closePath();
		this.context.stroke();			
	},
	drawPlayer: function(player) {
		//this.drawDebug(player);
		var image = this.getImage((player == this.localPlayer) ? 'localPlayer' : 'remotePlayer');
		var sc = player.parseSpriteSheet();
		
		this.context.drawImage(image, sc.x, sc.y, 100, 200, player.x - 50, player.y - 150, 100, 200);
	},
	
	drawPillar: function(pillar) {
		//this.drawDebug(pillar);
		this.context.drawImage(this.getImage("pillar"), pillar.x - 50, pillar.y - 250);
	},
	drawButton: function(button) {
		//this.drawDebug(button);
		if (button.pressed)
			this.context.drawImage(this.getImage("buttonPressed"), button.x - 50, button.y - 50);
		else
			this.context.drawImage(this.getImage("button"), button.x - 50, button.y - 50);
	},
	drawDoor: function(door) {
		//this.drawDebug(door);
		if (door.isOpen)
			this.context.drawImage(this.getImage("doorOpen"), door.x - 150, door.y - 250);
		else
			this.context.drawImage(this.getImage("doorClosed"), door.x - 150, door.y - 250);
	},
	
	
	updatePMaps: function() {
		// backgrounds
		this.layer0Context.drawImage(this.getImage("pmap0"), 0 ,0);
		this.layer1Context.drawImage(this.getImage("pmap1"), 0 ,0);
		
		//pillars
		for (var i=0; i < this.worldObjects.length; i++) {
			var obj = this.worldObjects[i];	
			var l = (obj.layer === 0) ? 1 : 0;
			switch(obj.type) {
				case 'pillar':
					// this function needs to redraw the black and white maps for a player to be able to hop to a pillar
					// it needs to draw on the OTHER layer fromt he one that the pillar is on	
					
					this["layer" + l + "Context"].fillStyle = "#fff";
					this["layer" + l + "Context"].fillRect(obj.x - 51, obj.y - 251, 102, 102);
					break;	
				// closed doors will prevent players from moving into them
				case 'door':
					if (!obj.isOpen) {
						this["layer" + l + "Context"].fillStyle = "#000";
						this["layer" + l + "Context"].fillRect(obj.x - 50, obj.y - 50, 100, 100);	
					}
					break;	
			}
		}		
	},
	
	// User input handlers
	// Keyboard
	keyPressed: function(e) {
		var dir = null;
		e.preventDefault();
		e.stopPropagation();
		switch(e.keyCode) {
			// need to write a player.move() function which does collision detection / moves any pillars
			// Rather, the engine should do the collision detection and tell the player and any pillars whether they need to move
			// need to make sure this doesn't interfere with what the other player is sending back through the server
			case 37: dir = 'left'; break; // left
			case 39: dir = 'right'; break; // right
			case 38: dir = 'up'; break; // up
			case 40: dir = 'down'; break; // down	
		}
		if (dir !== null) {
			if (this.verifyMove(this.localPlayer, dir, 100)) {
				// ensures the goal condition does not persist if the player leaves the goal
				if (this.goal) {
					this.goal = false;
					console.log("Goal left");
					socket.emit("leftGoal");
				}
				this.localPlayer.move(dir);
				socket.emit("move", {'object': 'localPlayer', 'dir': dir});
			}
			else if (this.verifyMove(this.localPlayer, dir, 200)) {
				this.localPlayer.hop(dir);
				socket.emit("hop", {'dir': dir});
			}
		}
	},
	//Mouse
	
	// Asset loading
	getImage: function(name) {
		return this.images[name];
	},
	// loads one image
	loadImage: function(url, name) {
		var image = new Image(),
			self = this;
		image.src = url;
		image.addEventListener('load', function(e) {
			self.imagesLoaded ++;
		});
		image.addEventListener('error', function(e) {
			self.imagesFailed ++;
		});
		this.images[name] = image;
	},
	// loads all images in the queue
	loadImages: function() {
		if (this.imagesIndex < this.imageUrls.length) {
			this.loadImage(this.imageUrls[this.imagesIndex].url, this.imageUrls[this.imagesIndex].name);
			this.imagesIndex ++;
		}
		return (this.imagesLoaded + this.imagesFailed) / this.imageUrls.length * 100;
	},
	// adds an image to the queue
	queueImage: function(url, name) {
		this.imageUrls.push({'url': url, 'name': name});
	},
	
	initImageAssets: function() {
		var self = this;
		//maps
		this.queueImage("../assets/maps/level" + this.world + "_layer0.png", 'map_layer0');
		this.queueImage("../assets/maps/level" + this.world + "_layer1.png", 'map_layer1');
		this.queueImage("../assets/playerMaps/level" + this.world + "_player0.png", 'pmap0');
		this.queueImage("../assets/playerMaps/level" + this.world + "_player1.png", 'pmap1');
		//sprites
		this.queueImage("../assets/sprites/pillar.png", "pillar");
		this.queueImage("../assets/sprites/switch_gold_up.png", "button");
		this.queueImage("../assets/sprites/switch_gold_down.png", "buttonPressed");
		this.queueImage("../assets/sprites/door_open.png", "doorOpen");
		this.queueImage("../assets/sprites/door_closed.png", "doorClosed");
		this.queueImage("../assets/sprites/blue_sprite_sheet.png", "localPlayer");
		this.queueImage("../assets/sprites/red_sprite_sheet.png", "remotePlayer");
		
		var loadingPercent = 0;
		var interval = setInterval(function(e) {
			loadingPercent = self.loadImages();
			$("#loadingBar").css("width", loadingPercent + "%");
			//document.getElementById('loadingpercent').innerHTML = 'Loading: ' + loadingPercent + '%';
			console.log("Images loading: " + loadingPercent + "%");
			if (loadingPercent == 100) {
				clearInterval(interval);
				ImagesDoneCallBack();
			}
		}, 16);
	}
}