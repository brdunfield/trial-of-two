var socket;
window.onload = function() {
	socket = io.connect('http://trialoftwo-thegirlandtheboy.rhcloud.com:8000/'); 
	//socket = io.connect('http://localhost:3250');
	//socket.emit("startConnection");
	
	// start screen
	$("#startButton").mouseup(function(){
		showFindPlayer();
		socket.emit("startConnection");
	});
	
	// Methods
	
	// Server methods
	socket.on('Test', function(data) {
		console.log(data + " Client ID: " + socket.socket.sessionid);
		socket.emit("playerJoined");
	});
	
	socket.on('playerJoined', function() {
		console.log("Server event: Player joined");
	});
	socket.on('playerLeft', function() {
		console.log("Server event: Player left");
	});
	
	// Set up game / level
	socket.on('startGame', function() {
		$("#startScreen").hide();
		console.log("Server event: Start Game");
		Game.start();
	});
	
	
	socket.on('initGame', function(dataObj) {
		showLoadingBar();
		Game = new Engine("canvas", dataObj);
		console.log("Server event: Init Game");
		console.log("Local layer: " + dataObj.playerIndex);
		Game.initImageAssets();
	});
	
	socket.on('endGame', function() {
		Game = null;
	});
	
	socket.on('gameWin', function() {
		winScreen();
	});
	
	socket.on('teammateGoal', function(msg) {
		console.log(msg);
	});
	
	socket.on('buttonPressed', function(dataObj) {
		Game.worldObjects[dataObj.index].activate();
		Game.activateDoors(dataObj.id);
		console.log("Button pressed event");
	});
	
	// Game methods
	socket.on('move', function(dataObj) {
		//var dir = data.dir;
		switch(dataObj.object) {
			case 'remotePlayer':
				Game.remotePlayer.move(dataObj.dir);
				console.log("Move event: Object: Remote Player is moving in direction: " + dataObj.dir);
				break;
			case 'worldObject':
				Game.worldObjects[dataObj.index].move(dataObj.dir);
				Game.remotePlayer.pushing = true;
				console.log("Move event: Object: WorldObject " + dataObj.index + " is moving in direction: " + dataObj.dir);
				break;
		}
	});
	socket.on('hop', function(dataObj) {
		Game.remotePlayer.hop(dataObj.dir);
		console.log("Move event: Object: Remote Player is hopping in direction: " + dataObj.dir);
	});
}

function ImagesDoneCallBack() {
	socket.emit("imagesLoaded");
}