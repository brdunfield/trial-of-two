$(document).ready(function(e) {
	var img = document.getElementById("bg");
	var animDistX = img.width - $(document).width()
	var animDistY = img.height - $(document).height()
	
	imgAnimLeft(animDistX, animDistY);
});

function imgAnimLeft(animDistX, animDistY){
	$("#bg").animate({
		left:-animDistX,
		top:animDistY/5*Math.random()*5
	}, 10000, function(){imgAnimRight(animDistX, animDistY);});
}

function imgAnimRight(animDistX, animDistY){
	$("#bg").animate({
		left:0,
		top:animDistY/5*Math.random()*5
	}, 10000, function(){imgAnimLeft(animDistX, animDistY);});
}

function animatePlayerFinderShrink(){
	$("#findPlayerLoader").animate({
		width:50,
		height:50
	},1000, function(){animatePlayerFinderGrow()});
}
function animatePlayerFinderGrow(){
	var rand = Math.random()*500+500;
	$("#findPlayerLoader").animate({
		width:rand,
		height:rand,
		"border-radius":rand
	},1000, function(){animatePlayerFinderShrink()});
}

function showFindPlayer(){
	$(".title").hide();
	$(".button").hide();
	$(".credits").hide();
	
	$("#findPlayer").show();
	animatePlayerFinderGrow();
}

function showLoadingBar(){
	$("#findPlayer").hide();
	
	$("#loadingBar").show();
}


function winScreen(){
	$("#winScreen").animate({
		top:30+"%"
	},1000);
}