var Player = function(layer, startx, starty) {
	var self = this;
	this.layer = layer;
	this.x = startx;
	this.y = starty;
	// moving
	this.vel = 200; // per second
	this.moving = false;
	this.isPushing = false;
	this.dir = 'down';
	
	this.animFrame = 0;
	this.anim = null;
	return this;
}

Player.prototype = {
	move: function(dir) {
		if (!this.moving) {
			var self = this;
			this.moving = true;
			this.dir = dir;
			this.anim = setInterval(function() {
				self.animFrame++;
				if (self.animFrame > 3) self.animFrame = 0;
			}, 75);
			setTimeout( function() {
				self.endMove();
			}, 500);
		}
	},
	hop: function(dir) {
		this.vel = 400;
		this.move(dir);
	},
	
	endMove: function() {
		this.vel = 200;
		this.moving = false;
		this.pushing = false;
		clearInterval(this.anim);
		thisanimFrame = 0;
		// round to nearest block of 100
		this.x = Math.round((this.x-50)/100) * 100 + 50;
		this.y = Math.round((this.y-50)/100) * 100 + 50;
	},
	
	parseSpriteSheet: function() {
		// parse SpriteSheet
		var x, y;
		if (this.pushing) {
			switch(this.dir) {
				case 'down':
					x = 400; y = 600;
					break;
				case 'up':
					x = 500; y = 600;
					break;
				case 'left':
					x = 700; y = 600;
					break;
				case 'right':
					x = 600; y = 600;
					break;
			}
			return { 'x' : x, 'y' : y }
		}
		switch(this.dir) {
			case 'down':
				x = 0; y = parseInt(this.animFrame*200) + 600;
				break;
			case 'up':
				x = 100; y = parseInt(this.animFrame*200) + 600;
				break;
			case 'left':
				x = 300; y = parseInt(this.animFrame*200) + 600;
				break;
			case 'right':
				x = 200; y = parseInt(this.animFrame*200) + 600;
				break;
		}
		return { 'x' : x, 'y' : y }
	},
	
}

var Pillar = function(layer, startx, starty) {
	var self = this;
	this.x = startx;
	this.y = starty;
	this.layer = layer;
	// moving
	this.vel = 200; // per second
	this.moving = false;
	this.movable = true;
	this.dir = null;
	
	this.type = 'pillar';
	
	return this;	
};

Pillar.prototype = new Player();
Pillar.prototype.constructor = Pillar;

var Button = function (x, y, layer, id) {
	this.x = x;
	this.y = y;
	this.layer = layer;
	this.pressed = false;
	this.linkedDoorsID = id;
	this.type = 'button';
}

Button.prototype = {
	activate: function() {
		this.pressed = true;
	},
	deactivate: function() {
		this.pressed = false;
	},
}

var Door = function(x, y, layer, isOpen, id) {
	this.x = x;
	this.y = y;
	this.linkedDoorsID = id;
	this.layer = layer;
	this.isOpen = isOpen;
	this.type = 'door';
}

Door.prototype = {
	activate: function() {
		this.isOpen = true;
	},
	deactivate: function() {
		this.isOpen = false;
	}
}