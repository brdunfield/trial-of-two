//#!/bin/env node

var handler = function(request, res) {
	console.log(request.url);
	
	// load html page special
	if (request.url == '/') request.url = 'test.html';
	fs.readFile('./' + request.url, Buffer, function (err, data) {
        if(err) throw err;
        if (request.url == 'test.html') {
			res.writeHead(200, {"Content-Type": "text/html"});
		} else {
			res.writeHead(200);
		}
		res.end(data);
    });
}

var app = require('http').createServer(handler);
var io = require('socket.io').listen(app);
//io.set('transports', [ 'websocket' ]);
var fs = require('fs');

//var port = 3250;
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

var connectedUsers = 0;
var loadedUsersInRooms = {};
var goalUsersInRooms = {};

//app.listen(port);
app.listen(port, ip);

// Methods
io.sockets.on('connection', function(socket) {
	
	connectedUsers ++;
	console.log("Player Joined");
	socket.emit("Test", "CONNECTION_SUCCESS");

	var roomName = "";
	socket.on("startConnection", function() {
		// assign the player to a room, or generate a new room if necessary
		var rooms = io.sockets.manager.rooms;
		var foundRoom = false;
		for (room in rooms) {
			if (room == "") continue;
			console.log(room);
			if (rooms[room].length < 2) {
				socket.join(room.replace('/', ""));
				roomName = room.replace('/', "");
				foundRoom = true;
				break;
			}
		}
		// if no room found, make a new one
		if (!foundRoom) {
			roomName = new Date().getTime();
			socket.join(roomName);
			loadedUsersInRooms[roomName] = 0;
			goalUsersInRooms[roomName] = 0;
		}
		console.log("Client joined room: " + roomName);	
		
		if (io.sockets.clients(roomName).length == 2) {
			io.sockets.clients(roomName).forEach(function(s, i) {
				// Game init:
				// - Assign player indexes and start coords
				// - Define which map to load
				// - Define any objects needing to be rendered and their start coords
				var dataObj = {};
				dataObj.playerIndex = i;
				dataObj.playerStartCoords = [{ 'x': 1650, 'y': 1250}, { 'x': 850, 'y': 1250}];
				dataObj.worldObjects = [{'type': 'pillar', 'x': 1550, 'y': 1050, 'layer': 0}, {'type': 'button', 'x': 1050, 'y': 350, 'layer': 1},
					{'type': 'door', 'x': 1550, 'y': 350, 'layer': 0, "isOpen": false}, {'type': 'door', 'x': 250, 'y': 150, 'layer': 1, "isOpen": true}];
				dataObj.world = 1;
				console.log("Init game to socket " + i);
				s.emit("initGame", dataObj);
			});
		}	
	});
	
	socket.on('disconnect', function() {
		connectedUsers --;
		loadedUsersInRooms[roomName] = 0;
		goalUsersInRooms[roomName] = 0;
		io.sockets.emit('playerLeft');
		console.log("Player Left");
		console.log("Loaded users: " + loadedUsersInRooms[roomName]);		
		io.sockets.in(roomName).emit('endgame');

	});
	
	socket.on('imagesLoaded', function() {
		loadedUsersInRooms[roomName] ++;
		console.log("Loaded users: " + loadedUsersInRooms[roomName]);
		if (loadedUsersInRooms[roomName] == 2) {
			io.sockets.in(roomName).emit('startGame');
		}
	});
	
	socket.on('playerJoined', function() {
		socket.broadcast.emit('playerJoined');
	});
	
	socket.on('buttonPressed', function(data) {
		console.log("Button pressed request from client " + socket.sessionid);
		socket.broadcast.to(roomName).emit('buttonPressed', data);
	}),
	
	socket.on('move', function(data) {
		console.log("Move Request from client " + socket.sessionid);
		
		switch(data.object) {
			// client server moved a local player, this needs to be broadcasted as a remote
			// player move for the other client
			case 'localPlayer':
				data.object = 'remotePlayer';
				break;
		}
		socket.broadcast.to(roomName).emit('move', data);
	});
	socket.on('hop', function(data) {
		console.log("Hop Request from client " + socket.sessionid);
		socket.broadcast.to(roomName).emit('hop', data);
	});
	
	// goal
	socket.on('reachedGoal', function() {
		goalUsersInRooms[roomName] ++;
		console.log("Goal reached: " + goalUsersInRooms[roomName] + " Users in goal");
		socket.broadcast.to(roomName).emit('teammateGoal', "Teammate reached goal");
		if (goalUsersInRooms[roomName] == 2) {
			console.log("Level Won!");
			io.sockets.in(roomName).emit('gameWin');
		}
	});
	socket.on('leftGoal', function() {
		goalUsersInRooms[roomName] --;
		socket.broadcast.to(roomName).emit('teammateGoal', "Teammate left goal")
		console.log("Goal left");
	});
});